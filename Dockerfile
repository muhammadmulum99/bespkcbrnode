FROM node:20.11.0

#membuat folder untuk app di server container
WORKDIR /usr/src/app

#mengcopy semua file package dengan extensi json
COPY package*.json ./

#untuk install npm package
RUN npm install

#membundle app source
COPY . .

EXPOSE 7070

CMD ["npm","run","start"]