module.exports = (sequelize, Sequelize) => {
    const Faktor = sequelize.define("faktor", {
      faktor_penyebab: {
        type: Sequelize.STRING
      },
      bobot_faktor: {
        type: Sequelize.STRING
      }
    });
  
    return Faktor;
  };