module.exports = (sequelize, Sequelize) => {
    const Kuesioner = sequelize.define("kuesioner", {
      id_result_cbr: {
        type: Sequelize.STRING
      },
      id_user: {
        type: Sequelize.STRING
      },
      id_pengetahuan: {
        type: Sequelize.BOOLEAN
      },
      nilai_cf: {
        type: Sequelize.BOOLEAN
      },
      nilai_user: {
        type: Sequelize.BOOLEAN
      }
    });
  
    return Kuesioner;
  };