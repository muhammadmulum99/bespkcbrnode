const { DatabasePengetahuan } = require('./datafix')

const database = DatabasePengetahuan
function combineCF(cf1, cf2) {
    return cf1 + cf2 * (1 - cf1);
}

// function calculateCF(database, inputan) {
//     const result = [];

//     for (let i = 0; i < database.length; i++) {
//         const penyakit = database[i];
//         const input = inputan.find(item => item.penyakit === penyakit.penyakit);

//         if (input) {
//             let cf_i = 1; // Default certainty factor for the disease

//             for (let j = 0; j < penyakit.gejala.length; j++) {
//                 if (penyakit.gejala[j] === input.gejala[j]) {
//                     cf_i *= penyakit.bobotKepastian[j];
//                 } else {
//                     cf_i *= 1 - penyakit.bobotKepastian[j];
//                 }
//             }

//             result.push({ penyakit: penyakit.penyakit, CF: (cf_i * 100) });
//         }
//     }

//     return result;
// }

function calculateCF(database, inputan) {
    let maxCF = 0; // Variabel untuk menyimpan nilai tertinggi
    let penyakitMaxCF = null; // Variabel untuk menyimpan penyakit dengan nilai tertinggi

    for (let i = 0; i < database.length; i++) {
        const penyakit = database[i];
        const input = inputan.find(item => item.penyakit === penyakit.penyakit);

        if (input) {
            let cf_i = 1; // Default certainty factor for the disease

            for (let j = 0; j < penyakit.gejala.length; j++) {
                if (penyakit.gejala[j] === input.gejala[j]) {
                    cf_i *= penyakit.bobotKepastian[j];
                } else {
                    cf_i *= 1 - penyakit.bobotKepastian[j];
                }
            }

            if (cf_i > maxCF) { // Jika nilai cf_i lebih besar dari maxCF, update maxCF dan penyakitMaxCF
                maxCF = cf_i;
                penyakitMaxCF = penyakit;
            }
        }
    }

    if (penyakitMaxCF) { // Jika ada penyakit dengan nilai tertinggi
        return [{ penyakit: penyakitMaxCF.penyakit, CF: (maxCF * 100) }];
    } else { // Jika tidak ada penyakit yang cocok
        return [];
    }
}
function calculateCF(database, inputan) {
    let maxCF = 0; // Variabel untuk menyimpan nilai tertinggi
    let penyakitMaxCF = null; // Variabel untuk menyimpan penyakit dengan nilai tertinggi

    for (let i = 0; i < database.length; i++) {
        const penyakit = database[i];
        const input = inputan.find(item => item.penyakit === penyakit.penyakit);

        if (input) {
            let cf_i = 1; // Default certainty factor for the disease

            for (let j = 0; j < penyakit.gejala.length; j++) {
                if (penyakit.gejala[j] === input.gejala[j]) {
                    cf_i *= penyakit.bobotKepastian[j];
                } else {
                    cf_i *= 1 - penyakit.bobotKepastian[j];
                }
            }

            if (cf_i > maxCF) { // Jika nilai cf_i lebih besar dari maxCF, update maxCF dan penyakitMaxCF
                maxCF = cf_i;
                penyakitMaxCF = penyakit;
            }
        }
    }

    if (penyakitMaxCF) { // Jika ada penyakit dengan nilai tertinggi
        return [{ penyakit: penyakitMaxCF.penyakit, CF: (maxCF * 100) }];
    } else { // Jika tidak ada penyakit yang cocok
        return [];
    }
}




// const cfResult = calculateCF(database, inputan);
// console.log("Faktor Kepastian (CF):");
// console.log(cfResult)

module.exports = {
    calculateCF
};







