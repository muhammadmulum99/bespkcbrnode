module.exports = {
    HOST: "172.17.0.3",
    USER: "root",
    PASSWORD: "root",
    DB: "spk_db",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };