const db = require("../models");
const Pengetahuan = db.pengetahuan;
const Op = db.Sequelize.Op;

// Create and Save a new Pengetahuan
exports.create = (req, res) => {
    // Validate request
    if (!req.body.id_faktorInput || !req.body.diagnosisInput) {
      res.status(400).send({
        message: "Content can not be empty!"
      });
      return;
    }
  
    const foto= "foto.png"
    // Create a Pengetahuan
    const Pengetahuans = {
      id_faktor: req.body.id_faktorInput,
      diagnosisId: req.body.diagnosisIdInput,
      diagnosa: req.body.diagnosisInput,
      BobotKepastian: req.body.bobotKepastianInput
   
    };
  
    // Save Pengetahuan in the database
    Pengetahuan.create(Pengetahuans)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the Pengetahuan."
        });
      });
  };

// Retrieve all Pengetahuan from the database.
exports.findAll = (req, res) => {
    const nama_Pengetahuan = req.query.diagnosa;
    var condition = nama_Pengetahuan ? { diagnosa: { [Op.like]: `%${nama_Pengetahuan}%` } } : null;
    Pengetahuan.findAll({ where: condition })
    // Pengetahuan.findAll()
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Pengetahuan."
        });
      });
  };

// Find a single Pengetahuan with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Pengetahuan.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find Pengetahuan with id=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Pengetahuan with id=" + id
        });
      });
  };

// Update a Pengetahuan by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
  
    Pengetahuan.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Pengetahuan was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Pengetahuan with id=${id}. Maybe Pengetahuan was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Pengetahuan with id=" + id
        });
      });
  };

// Delete a Pengetahuan with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
  
    Pengetahuan.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Pengetahuan was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Pengetahuan with id=${id}. Maybe Pengetahuan was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Pengetahuan with id=" + id
        });
      });
  };

// Delete all Pengetahuan from the database.
exports.deleteAll = (req, res) => {
    Pengetahuan.destroy({
      where: {},
      truncate: false
    })
      .then(nums => {
        res.send({ message: `${nums} Pengetahuan were deleted successfully!` });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all Pengetahuan."
        });
      });
  };


  exports.findAllQuery = (req, res) => {

    const d=`SELECT *FROM pengetahuans p JOIN faktors f ON FIND_IN_SET(f.id, REPLACE(REPLACE(p.id_faktor, '[', ''), ']', '')) > 0 JOIN penyakits ps ON ps.id=p.diagnosisId;`
    const query='select * from pengetahuans as a join penyakits b on b.id=a.diagnosisId'
    db.sequelize.query(d,{ type: db.sequelize.QueryTypes.SELECT })
    // Pengetahuan.findAll()
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving Pengetahuan."
        });
      });
  };