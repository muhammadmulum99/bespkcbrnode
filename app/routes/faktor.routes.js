const { authJwt } = require("../middleware");
const controller = require("../controllers/user.controller");
// const faktor = require("../controllers/faktor.controller");
const faktor=require("../controllers/faktor.controller")

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });

  app.get("/api/be/faktor",faktor.findAll);

  app.post("/api/be/faktor",faktor.create);

  app.get("/api/be/faktor/:id",faktor.findOne);

  app.put("/api/be/faktor/:id",faktor.update);

  app.delete("/api/be/faktor/:id",faktor.delete)

  // app.delete("/api/be/faktor/:id",[authJwt.verifyToken],faktor.delete)



  //app.get("/api/be/faktor",faktor.findAll);

  //app.get("/api/be/faktor", controller.findAll);

//   app.get(
//     "/api/test/faktor",
//     [authJwt.verifyToken],
//     controller.userBoard
//   );

};